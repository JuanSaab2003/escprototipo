using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.InputSystem;
//using UnityEngine.InputSystem.Composites;

public class Combo : MonoBehaviour
{
    // Referencias externas
    private Animator animatorObj;
    private CharacterController characterController;


    // Declaracion de variables
    public bool attack;
    int numPresses = 0;
    float lastPressTime = 0;
    float maxComboDelay = 0.2f;

    //[Header("AttackVariables")]
    //[SerializeField] private float attackDamage = 1000;
    //[SerializeField] private float attackRadius = 2;
    //[SerializeField] private float attackDistance = 2;
    //[SerializeField] private LayerMask layerToDetect;

    private void Start()
    {
        // Instanciar animator en el script
        animatorObj = GetComponent<Animator>();
        characterController = GetComponent<CharacterController>();
    }

    /*
    private void Awake()
    {
        input = new PlayerInput();

        input.CharacterControls.Attack.started += ctx =>
        {
            attackPressed = ctx.ReadValueAsButton();

        };

        input.CharacterControls.Attack.canceled += ctx =>
        {
            attackPressed = ctx.ReadValueAsButton();

        };

    }
    */
    void Update()
    {
        // Si es que ha pasado el 75% de la animacion actual, pone su bool (animador) a falso
        if (animatorObj.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.8f && animatorObj.GetCurrentAnimatorStateInfo(0).IsName("attack1"))
        {
            animatorObj.SetBool("isAttacking1", false);
        }
        if (animatorObj.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.8f && animatorObj.GetCurrentAnimatorStateInfo(0).IsName("attack2"))
        {
            animatorObj.SetBool("isAttacking2", false);
        }
        if (animatorObj.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.8f && animatorObj.GetCurrentAnimatorStateInfo(0).IsName("attack3"))
        {
            animatorObj.SetBool("isAttacking3", false);
            // Luego de acabar el 3er ataque del combo, resetea el combo a 0
            numPresses = 0;
        }


        // Si es que ha pasado cierto tiempo desde que se presiono el boton, se resetea el combo a 0
        if (Time.time - lastPressTime > maxComboDelay)
        {
            numPresses = 0;
        }


        // Comprueba si se presiona boton para atacar y llamar funcion encargada de combo

        if (attack)
        {
            onPress();
        }

    }


    // Mira cuantas veces se ha presionado el boton y si termino una animacion, pone la siguiente
    void onPress()
    {
        lastPressTime = Time.time;
        numPresses++;
        if (numPresses == 1)
        {
            animatorObj.SetBool("isAttacking1", true);
        }
        numPresses = Mathf.Clamp(numPresses, 0, 3);

        if (numPresses >= 2 && animatorObj.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.8f && animatorObj.GetCurrentAnimatorStateInfo(0).IsName("attack1"))
        {
            animatorObj.SetBool("isAttacking1", false);
            animatorObj.SetBool("isAttacking2", true);
        }
        if (numPresses >= 3 && animatorObj.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.8f && animatorObj.GetCurrentAnimatorStateInfo(0).IsName("attack2"))
        {
            animatorObj.SetBool("isAttacking1", false);
            animatorObj.SetBool("isAttacking2", false);
            animatorObj.SetBool("isAttacking3", true);
        }
    }

    /*
    void OnEnable()
    {
        input.CharacterControls.Enable();
    }
    void OnDisable()
    {
        input.CharacterControls.Disable();
    }
    */

    //void Attack()
    //{
    //    //Creaci�n rayo (Quiero que ataque hacia su eje delantero)
    //    Ray attackRay = new Ray(transform.position, transform.forward);

    //    //Detecto todos los enemigos que est�n dentro del radio de ataque
    //    RaycastHit[] hits = Physics.SphereCastAll(attackRay, attackRadius, attackDistance, layerToDetect, QueryTriggerInteraction.Ignore);
    //    //Compruebo que hay al menos un enemigo dentro del radio
    //    if (hits.Length > 0)
    //    {
    //        //Bucle por todos los enemigos detectados
    //        for (int i = 0; i < hits.Length; i++)
    //        {
    //            //Obtengo y compruebo que el objeto tiene el component EnemyBase
    //            EnemyBase enemy = hits[i].transform.GetComponent<EnemyBase>();
    //            if (enemy != null)
    //            {
    //                //Si todo se cumple, le aplico da�o
    //                enemy.ReceiveDamage(attackDamage);
    //            }
    //        }
    //    }
    //}
}