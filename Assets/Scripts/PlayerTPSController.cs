using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.Events;

public class PlayerTPSController : MonoBehaviour
{
    public Camera cam;
    //public UnityEvent onInteractionInput;
    private InputData input;
    private CharacterAnimBasedMovement characterMovement;

    public bool blockInput { get; set; }

    private Combo combo;

    public bool onInteractionZone { get; set; }

    public static event Action onInteractionInput;

    void Start()
    {
        characterMovement = GetComponent<CharacterAnimBasedMovement>();
        combo = GetComponent<Combo>();
    }

    void Update()
    {

        if (blockInput)
        {
            input.resetInput();
        }
        else
        {
            input.getInput();
        }

        if (onInteractionZone && input.jump)
        {
            if (onInteractionInput != null)
            {
                onInteractionInput?.Invoke();
            }
            /*
            else
            {
                // Get input from player
                input.getInput();

                // Move the character
                characterMovement.moveCharacter(input.hMovement, input.vMovement, cam, input.jump, input.dash);

                combo.attack = input.attack;
            }
            */
        }
        // Get input from player
        input.getInput();

        // Move the character
        characterMovement.moveCharacter(input.hMovement, input.vMovement, cam, input.jump, input.dash);

        combo.attack = input.attack;
    }
}
